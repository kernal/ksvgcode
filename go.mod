module gitlab.com/kernal/ksvgcode

go 1.19

require (
	github.com/boombuler/barcode v1.0.1
	github.com/go-chi/chi/v5 v5.0.7
	github.com/tunabay/go-bitarray v1.3.1
	github.com/tunabay/go-bmppath v0.1.1
	golang.org/x/image v0.0.0-20220902085622-e7cb96979f69
	golang.org/x/net v0.0.0-20220826154423-83b083e8dc8b
)

require golang.org/x/text v0.3.7 // indirect

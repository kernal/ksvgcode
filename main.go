package main

import (
	"context"
	"flag"
	"image"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/boombuler/barcode/qr"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/tunabay/go-bitarray"
	"github.com/tunabay/go-bmppath"
	"golang.org/x/image/draw"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type config struct {
	bindAddr string
	cacheAge string
}

var c config

func readConfig() {
	var cacheAge time.Duration
	flag.StringVar(&c.bindAddr, "a", "localhost:8000", "Address to bind (ip:port)")
	flag.DurationVar(&cacheAge, "c", 8760 * time.Hour, "Cache-Control header max-age")
	flag.Parse()
	c.cacheAge = strconv.Itoa(int(cacheAge.Seconds()))
}

func middlewareServerHeader(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", "ksvgcode/0.0.1")
		next.ServeHTTP(w, r)
	})
}

func imageToBitArray(img image.Image) *bitarray.BitArray {
	bounds := img.Bounds()
	rgba := image.NewRGBA(bounds)
    draw.Copy(rgba, image.Point{}, img, bounds, draw.Src, nil)
	size := bounds.Size()
	length := size.X * size.Y
	buf := bitarray.NewBufferFromBitArray(bitarray.NewZeroFilled(length))
	for i := 0; i < length * 4 ; i += 4 {
		if rgba.Pix[i] == 0 {
			buf.PutBitAt(i / 4, 1) // black
		}
	}
	return buf.BitArray()
}

func writeSVG(w io.Writer, p *bmppath.Path) (err error) {
	width, height := strconv.Itoa(p.Width), strconv.Itoa(p.Height)
	if _, err = w.Write([]byte(`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ` + width + " " +
            height + `"><path fill="#fff" d="M0 0h` + width + `v` + height + `H0z"/><path d="`)); err != nil {
		return
	}
	if err = p.WriteSVGD(w); err != nil {
		return
	}
	_, err = w.Write([]byte(`"/></svg>`))
	return
}

func qrHandler(w http.ResponseWriter, r *http.Request) {
	content := r.URL.Query().Get("content")
        errorCorrectionLevel := qr.M
        errorCorrectionLevelStr := r.URL.Query().Get("errorCorrectionLevel")
        if errorCorrectionLevelStr != "" {
                i, err := strconv.ParseInt(errorCorrectionLevelStr, 10, 64)
                if err != nil {
		        http.Error(w, "strconv.ParseInt", http.StatusInternalServerError)
                        return
                }
                if i < 0 || i > 3 {
		        http.Error(w, "errorCorrectionLevel", http.StatusInternalServerError)
                        return
                }
                errorCorrectionLevel = qr.ErrorCorrectionLevel(i)
        }
	img, err := qr.Encode(content, errorCorrectionLevel, qr.Auto)
	if err != nil {
		log.Println(err)
		http.Error(w, "qr.Encode", http.StatusInternalServerError)
        return
	}
	bmp := bitarray.NewBufferFromBitArray(imageToBitArray(img))
	path, err := bmppath.New(bmp, img.Bounds().Size().X)
	if err != nil {
		log.Println(err)
		http.Error(w, "bmppath.New", http.StatusInternalServerError)
        return
	}
	w.Header().Set("Content-Type", "image/svg+xml")
	w.Header().Set("Cache-Control", "public,max-age=" + c.cacheAge +",immutable")
	if err := writeSVG(w, path); err != nil {
		log.Println(err)
		http.Error(w, "path.WriteSVG", http.StatusInternalServerError)
	}
}


func initRouter() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middlewareServerHeader)
	r.Get("/qr", qrHandler)
	return r
}

func runServer() {
	h2s := &http2.Server{}
	srv := &http.Server{
		Addr: c.bindAddr,
		Handler: h2c.NewHandler(initRouter(), h2s),
	}
	serverCtx, serverStopCtx := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig
		shutdownCtx, _ := context.WithTimeout(serverCtx, 30 * time.Second)
		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("Graceful shutdown timed out... Forcing exit.")
			}
		}()
		err := srv.Shutdown(shutdownCtx)
		if err != nil {
			log.Fatal(err)
		}
		serverStopCtx()
	}()
	err := srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-serverCtx.Done()
}

func main() {
	readConfig()
	runServer()
}
